<?php

use App\Models\Categeory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('home',['cat'=>Categeory::getCategiries()]);
//})->name('home');
Route::get('/categori/{catslug}','\App\Http\Controllers\DetailController@getDetailByCategory')->name('article');
Route::get('/detail/{slug}','\App\Http\Controllers\DetailController@getDetailBySlug')->name('detail');
Route::get('/mypage',[App\Http\Controllers\UserController::class,'index'])->name('mypage');
//Route::post('/detail/{slug}/cometing/',[App\Http\Controllers\DetailController::class ,'postComent'])->name('postcoment');
Route::post('/categori/addbasket/{slug}',[\App\Http\Controllers\BasketController::class,'cretaeBasket'])->name('addbasket');
Route::get('/basket',[\App\Http\Controllers\BasketController::class,'index'])->name('basket');
Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
