<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Facades\FormElement;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Section;

/**
 * Class DetailSection
 *
 * @property \App\Models\Detail $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class DetailSection extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title='Детали';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $display = AdminDisplay::datatablesAsync()
            ->setDisplaySearch(true);
        $display->setColumns(
            AdminColumn::link('name', 'Название')->setWidth('200px'),
            AdminColumn::text('slug','Символьный код'),
            AdminColumn::text('price','Цена'),
            AdminColumn::text('brif','Краткое описание'),
            AdminColumn::image('image','Картинка')->setWidth('150px')
        )->setOrder(1, 'desc')
            ->paginate(30);
        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel();
        $tabs = AdminDisplay::tabbed([
            'Карточка в каталоге'=> new FormElements([
                    AdminFormElement::text('name','Название детали')->required(),
                    AdminFormElement::text('slug','Символьный код')->required(),
                    AdminFormElement::text('price','Цена')->required(),
                    AdminFormElement::text('brif','Краткое описание'),
                    AdminFormElement::image('image','Картинка'),
            ]),
            'Категории'=> new FormElements([
                AdminFormElement::multiselect('categories', 'Категория', 'App\Models\Categeory')
                    ->setDisplay('name')
                    ->required()
            ]),
            'Детальное описание'=> new FormElements([
                AdminFormElement::wysiwyg('content', 'Детальное описание')
                    ->setHeight(1000)
                    ->required()
            ])
        ]);
        $form->addElement($tabs);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
    public function getIcon()
    {
        return 'fa fa-cogs';
    }
}
