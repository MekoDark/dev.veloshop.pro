<?php

namespace App\Http\Controllers;

use App\Models\Categeory;
use App\Models\Coment;
use App\Models\Detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DetailController extends Controller
{
    public function getDetailByCategory(Request $request)
    {
        $article = Categeory::getDetails($request->catslug);
        return view('home', ['cat' => Categeory::getCategiries(), 'details' => $article]);
    }
    public  function getDetailBySlug(Request $request){
        $dtail=Detail::getBySlug($request->slug);
       return view('page',['data'=>$dtail]);
    }

}
