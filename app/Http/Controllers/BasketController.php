<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\Detail;
use App\Models\User;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    public  function index(){
        return view('pagebasket' ,['data'=>User::getBasket()]);
   }
    public function cretaeBasket(Request $request)
    {
        if (empty(User::getBasket())) {
            $basket = User::getBasket();
            $basket->user()->attach(User::getUserById()->get('id'));
            $basket->detail()->attach(Detail::getBySlug($request->slug)->get('id'));
        } else {
            $basket = Basket::create();
           // dd( $basket->user()->attach(User::getUserById()->get('id')));
            $basket->user()->attach(User::getUserById()->get('id'));
            $basket->detail()->attach(Detail::getBySlug($request->slug)->get('id'));
        }
        return view('pagebasket' ,['data'=>User::getBasket()]);
    }
}
