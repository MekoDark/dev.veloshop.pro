<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use function Ramsey\Uuid\v1;

class UserController extends Controller
{
    public function index(){
//        dd(User::getUserById());
        return view('userpage',['date'=>User::getUserById()]);
    }
}
