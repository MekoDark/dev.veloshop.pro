<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categeory extends Model
{
    use HasFactory;

    static function getCategori($slug){
        return self::query()->where('slug',$slug)->first();
    }
    static  function getCategiries(){
        $Categeri=collect([]);
        self::query()->get()->map(function ($item) use($Categeri){
            $dataset=new \stdClass();
            $dataset->name=$item->name;
            $dataset->count=$item->detail->count();
            $dataset->slug=$item->slug;
            $Categeri->push($dataset);
        });
        return $Categeri;
//        return self::query()->detail()->get();
    }
    public  function detail(){
        return $this->belongsToMany(Detail::class);
    }
     static function getDetails($slug){
        return self::getCategori($slug)->detail()->get();
     }
}
