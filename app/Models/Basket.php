<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    public function detail(){
        return $this->belongsToMany(Detail::class);
    }

    public function user(){
      return  $this->belongsToMany(User::class);
    }
}
