<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    public function getRouteKeyName()
    {
        return ['slug'];

    }

    static function getBySlug($slug){

           return self::query()->where('slug',$slug)->first();

    }



    public function categories(){
        return $this->belongsToMany(Categeory::class);
    }

   public  function  basket(){
        $this->belongsToMany(Basket::class);
   }
}
