<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategeryDetais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categeory_detail', function (Blueprint $table) {
            $table->bigInteger('categeory_id')
                ->unsigned()
                ->index();
            $table->foreign('categeory_id')
                ->references('id')
                ->on('categeories')
                ->onDelete('cascade');

            $table->bigInteger('detail_id')
                ->unsigned()
                ->index();
            $table->foreign('detail_id')
                ->references('id')
                ->on('details')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categery_detais');
    }
}
