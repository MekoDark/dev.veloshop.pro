<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basket_detail', function (Blueprint $table) {
            $table->bigInteger('basket_id')
                ->unsigned()
                ->index();
            $table->foreign('basket_id')
                ->references('id')
                ->on('baskets')
                ->onDelete('cascade');

            $table->bigInteger('detail_id')
                ->unsigned()
                ->index();
            $table->foreign('detail_id')
                ->references('id')
                ->on('details')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basket_detail');
    }
}
