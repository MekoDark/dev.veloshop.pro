@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <img src="{{asset($data->image)}}" class="col-4">
            <div class="card col-8" >
                {!! $data->content !!}
            </div>
        </div>
        @guest()

        @else
            <form method="POST" action="">

            </form>
        @endguest
    </div>
@endsection
