@extends('layouts.app');
@section('content')
    @isset($data)
        @foreach($data as $detail)
            <div class="card" style="width: 18rem; margin: 1px">
                <img src="{{asset($detail->detail()->pluck('image'))}}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">{{$detail->detail()->pluck('name')}}</h5>
                    <p class="card-text">{{$detail->detail()->pluck('brif')}}</p>
                </div>
                </div>
        @endforeach
    @endisset
@endsection
