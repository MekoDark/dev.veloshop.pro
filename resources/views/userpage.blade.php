@extends('layouts.app')
@section('content')
    <div class="d-flex position-relative">
        <img src="{{asset('images/user.png')}}" class="flex-shrink-0 me-3" alt="...">
        <div>
            <h5 class="mt-0">{{$date->name}}</h5>
            <h5 class="mt-0">{{$date->surname}}</h5>
        </div>
    </div>
@endsection
