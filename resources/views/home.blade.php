@extends('layouts.app')

@section('content')
    <ul class="list-group list-group-flush col-4">
      @foreach($cat as $data)
            <li class="list-group-item"><a class="href" href="{{route('article',$data->slug)}}">{{$data->name}}</a> <span class="badge bg-dark">{{$data->count}}</span></li>
        @endforeach
    </ul>
    <div class="col-8">
      @if(!empty($details))
            @foreach($details as $detail)
                <div class="card" style="width: 18rem;">
                    <img src="{{asset($detail->image)}}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">{{$detail->name}}</h5>
                        <p class="card-text">{{$detail->brif}}</p>
                    </div>
                    <div class="card-body row">
                        <a href="{{route('detail',$detail->slug)}}" class="btn btn-primary col-5">Перейти</a>
                        <p class="card-text col-3">{{$detail->price}}</p><form method="POST" action="{{route('addbasket',$detail->slug)}}">@csrf<button type="submit" class="btn btn-primary">
                               В корзину
                            </button></form>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection
