<?
$steps = $model->steps ? $model->steps : json_encode([]);
?>
<service-steps
        db_service="{{$model->toJson()}}"
        name_input="{{$name_input}}"
        db_steps="{{$steps}}">
</service-steps>