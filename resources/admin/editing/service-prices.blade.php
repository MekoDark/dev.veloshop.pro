<?
$prices = $model->prices ? $model->prices : json_encode([]);
?>
<service-prices
        db_service="{{$model->toJson()}}"
        name_input="{{$name_input}}"
        db_prices="{{$prices}}">
</service-prices>