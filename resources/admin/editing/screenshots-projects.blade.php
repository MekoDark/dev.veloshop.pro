<?
$screenshots_structure = $model->screenshots_structure ? $model->screenshots_structure : json_encode([]);
?>
<screenshots-projects
        db_project="{{$model->toJson()}}"
        name_input="{{$name_input}}"
        db_screenshots_structure="{{$screenshots_structure}}">
</screenshots-projects>