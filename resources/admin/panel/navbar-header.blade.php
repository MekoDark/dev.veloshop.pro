<li>
    <a href="{{route('admin.cache_clear')}}">
        <i class="fa fa-btn fa-trash"></i> Очистить кеш
    </a>
</li>
<li>
    <a href="{{route('home')}}" target="_blank">
        <i class="fa fa-chrome"></i>
        Webest.media
    </a>

    <div id="SVG_container"></div>
    <script>
        (function(window, document) {
            "use strict";

            var file = "/img/sprite/sprite.svg",
                revision = 1;

            if (
                !document.createElementNS ||
                !document.createElementNS("http://www.w3.org/2000/svg", "svg")
                    .createSVGRect
            )
                return true;

            var isLocalStorage =
                    "localStorage" in window && window["localStorage"] !== null,
                request,
                data,
                SVG_container = document.getElementById("SVG_container"),
                insertIT = function() {
                    SVG_container.insertAdjacentHTML("afterbegin", data);
                },
                insert = function() {
                    if (document.body) insertIT();
                    else document.addEventListener("DOMContentLoaded", insertIT);
                };

            if (isLocalStorage && localStorage.getItem("inlineSVGrev") == revision) {
                data = localStorage.getItem("inlineSVGdata");
                if (data) {
                    insert();
                    return true;
                }
            }

            try {
                request = new XMLHttpRequest();
                request.open("GET", file, true);
                request.onload = function() {
                    if (request.status >= 200 && request.status < 400) {
                        data = request.responseText;
                        insert();
                        if (isLocalStorage) {
                            localStorage.setItem("inlineSVGdata", data);
                            localStorage.setItem("inlineSVGrev", revision);
                        }
                    }
                };
                request.send();
            } catch (e) {}
        })(window, document);
    </script>
</li>

<li class="dropdown user user-menu" style="margin-right: 20px;">
    @if($user)
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
            <span class="hidden-xs">{{ $user->name }}</span>
        </a>
        <ul class="dropdown-menu">
            <li class="user-header" style="height: auto;">
                <p>
                    {{ $user->name }}
                    <small>@lang('sleeping_owl::lang.auth.since', ['date' => $user->created_at->format('d.m.Y')])</small>
                </p>
            </li>
            <li class="user-footer">
                <a href="/logout"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fa-btn fas fa-sign-out-alt"></i> @lang('sleeping_owl::lang.auth.logout')
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </li>
        </ul>
    @endif
</li>